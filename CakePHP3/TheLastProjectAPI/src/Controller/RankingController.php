<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ranking Controller
 *
 * @property \App\Model\Table\RankingTable $Ranking
 *
 * @method \App\Model\Entity\Ranking[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RankingController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $ranking = $this->paginate($this->Ranking);

        $this->set(compact('ranking'));
    }

    /**
     * View method
     *
     * @param string|null $id Ranking id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ranking = $this->Ranking->get($id, [
            'contain' => [],
        ]);

        $this->set('ranking', $ranking);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $ranking = $this->Ranking->newEntity();
        if ($this->request->is('post')) {
            $ranking = $this->Ranking->patchEntity($ranking, $this->request->getData());
            if ($this->Ranking->save($ranking)) {
                $this->Flash->success(__('The ranking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ranking could not be saved. Please, try again.'));
        }
        $this->set(compact('ranking'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Ranking id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ranking = $this->Ranking->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ranking = $this->Ranking->patchEntity($ranking, $this->request->getData());
            if ($this->Ranking->save($ranking)) {
                $this->Flash->success(__('The ranking has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ranking could not be saved. Please, try again.'));
        }
        $this->set(compact('ranking'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Ranking id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $ranking = $this->Ranking->get($id);
        if ($this->Ranking->delete($ranking)) {
            $this->Flash->success(__('The ranking has been deleted.'));
        } else {
            $this->Flash->error(__('The ranking could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function setRanking()
	{
		$this->autoRender	= false;

        //POST パラメータを取得
        $postName   = $this->request->data["name"];
        $postclearseconds  = $this->request->data["clearseconds"];

        $record = array(
            "Name"=>$postName,
            "ClearSeconds"=>$postclearseconds,
            //"date"=>date("Y/m/d H:i:s")
        );

        //テーブルにレコードを追加
        $prm1    = $this->Ranking->newEntity();
        $prm2    = $this->Ranking->patchEntity($prm1,$record);
        if( $this->Ranking->save($prm2) ){
            //echo "1";   //成功
           echo"1";
//テーブルからランキングリストをとってくる
//$query	= $this->Ranking->find("all");

//クエリー処理を行う。
//$query->order(['ClearSeconds'=>'DESC']);   //降順
//$query->limit(10);                  //取得件数を10件までに絞る

//jsonにシリアライズする。
//$json	= json_encode($query);

//jsonデータを返す。（レスポンスとして表示する。）
//echo $json;

        }else{
            echo "0";   //失敗
        }
    }
    public function getRankings()
	{
		$this->autoRender	= false;
		
		//テーブルからランキングリストをとってくる
        $query	= $this->Ranking->find("all");

        //クエリー処理を行う。
        $query->order(['ClearSeconds'=>'ASC']);   //降順
        $query->limit(10);                  //取得件数を10件までに絞る
		
		//jsonにシリアライズする。
		$json	= json_encode($query);

		//jsonデータを返す。（レスポンスとして表示する。）
		echo $json;
    }
}
