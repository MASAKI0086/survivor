<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ranking $ranking
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ranking'), ['action' => 'edit', $ranking->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ranking'), ['action' => 'delete', $ranking->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ranking->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ranking'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ranking'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ranking view large-9 medium-8 columns content">
    <h3><?= h($ranking->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($ranking->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ranking->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ClearSeconds') ?></th>
            <td><?= $this->Number->format($ranking->ClearSeconds) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($ranking->date) ?></td>
        </tr>
    </table>
</div>
