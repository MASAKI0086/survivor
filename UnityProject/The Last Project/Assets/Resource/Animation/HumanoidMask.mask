%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: HumanoidMask
  m_Mask: 01000000010000000100000000000000000000000100000001000000010000000100000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Ironman
    m_Weight: 1
  - m_Path: Ironman/b_Root
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_LeftUpLeg
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_LeftUpLeg/b_LeftLeg
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_LeftUpLeg/b_LeftLeg/b_LeftFoot
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_LeftUpLeg/b_LeftLeg/b_LeftFoot/b_LeftToeBase
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_LeftUpLeg/b_LeftLeg/b_LeftFoot/b_LeftToeBase/b_LeftToeBase_End
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_LeftUpLeg/b_LeftLeg/b_LeftFoot/b_LeftToeBase/b_LeftToeBase_End/b_LeftToeBase_End_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_LeftUpLeg/b_LeftLeg/b_LeftFoot/b_LeftToeBase/b_LeftToeBase_End/b_LeftToeBase_End_end/b_LeftToeBase_End_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_RightUpLeg
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_RightUpLeg/b_RightLeg
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_RightUpLeg/b_RightLeg/b_RightFoot
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_RightUpLeg/b_RightLeg/b_RightFoot/b_RightToeBase
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_RightUpLeg/b_RightLeg/b_RightFoot/b_RightToeBase/b_RightToeBase_End
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_RightUpLeg/b_RightLeg/b_RightFoot/b_RightToeBase/b_RightToeBase_End/b_RightToeBase_End_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_RightUpLeg/b_RightLeg/b_RightFoot/b_RightToeBase/b_RightToeBase_End/b_RightToeBase_End_end/b_RightToeBase_End_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandIndex1
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandIndex1/b_LeftHandIndex2
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandIndex1/b_LeftHandIndex2/b_LeftHandIndex3
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandIndex1/b_LeftHandIndex2/b_LeftHandIndex3/b_LeftHandIndex4
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandIndex1/b_LeftHandIndex2/b_LeftHandIndex3/b_LeftHandIndex4/b_LeftHandIndex4_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandIndex1/b_LeftHandIndex2/b_LeftHandIndex3/b_LeftHandIndex4/b_LeftHandIndex4_end/b_LeftHandIndex4_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandMiddle1
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandMiddle1/b_LeftHandMiddle2
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandMiddle1/b_LeftHandMiddle2/b_LeftHandMiddle3
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandMiddle1/b_LeftHandMiddle2/b_LeftHandMiddle3/b_LeftHandMiddle4
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandMiddle1/b_LeftHandMiddle2/b_LeftHandMiddle3/b_LeftHandMiddle4/b_LeftHandMiddle4_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandMiddle1/b_LeftHandMiddle2/b_LeftHandMiddle3/b_LeftHandMiddle4/b_LeftHandMiddle4_end/b_LeftHandMiddle4_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandPinky1
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandPinky1/b_LeftHandPinky2
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandPinky1/b_LeftHandPinky2/b_LeftHandPinky3
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandPinky1/b_LeftHandPinky2/b_LeftHandPinky3/b_LeftHandPinky4
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandPinky1/b_LeftHandPinky2/b_LeftHandPinky3/b_LeftHandPinky4/b_LeftHandPinky4_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandPinky1/b_LeftHandPinky2/b_LeftHandPinky3/b_LeftHandPinky4/b_LeftHandPinky4_end/b_LeftHandPinky4_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandRing1
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandRing1/b_LeftHandRing2
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandRing1/b_LeftHandRing2/b_LeftHandRing3
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandRing1/b_LeftHandRing2/b_LeftHandRing3/b_LeftHandRing4
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandRing1/b_LeftHandRing2/b_LeftHandRing3/b_LeftHandRing4/b_LeftHandRing4_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandRing1/b_LeftHandRing2/b_LeftHandRing3/b_LeftHandRing4/b_LeftHandRing4_end/b_LeftHandRing4_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandThumb1
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandThumb1/b_LeftHandThumb2
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandThumb1/b_LeftHandThumb2/b_LeftHandThumb3
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandThumb1/b_LeftHandThumb2/b_LeftHandThumb3/b_LeftHandThumb4
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandThumb1/b_LeftHandThumb2/b_LeftHandThumb3/b_LeftHandThumb4/b_LeftHandThumb4_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftHandThumb1/b_LeftHandThumb2/b_LeftHandThumb3/b_LeftHandThumb4/b_LeftHandThumb4_end/b_LeftHandThumb4_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftWeapon
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftWeapon/b_LeftWeapon_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_LeftShoulder/b_LeftArm/b_LeftArmRoll/b_LeftForeArm/b_LeftForeArmRoll/b_LeftHand/b_LeftWeapon/b_LeftWeapon_end/b_LeftWeapon_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_Neck
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_Neck/b_head
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_Neck/b_head/b_head_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_Neck/b_head/b_head_end/b_head_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandIndex1
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandIndex1/b_RightHandIndex2
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandIndex1/b_RightHandIndex2/b_RightHandIndex3
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandIndex1/b_RightHandIndex2/b_RightHandIndex3/b_RightHandIndex4
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandIndex1/b_RightHandIndex2/b_RightHandIndex3/b_RightHandIndex4/b_RightHandIndex4_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandIndex1/b_RightHandIndex2/b_RightHandIndex3/b_RightHandIndex4/b_RightHandIndex4_end/b_RightHandIndex4_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandMiddle1
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandMiddle1/b_RightHandMiddle2
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandMiddle1/b_RightHandMiddle2/b_RightHandMiddle3
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandMiddle1/b_RightHandMiddle2/b_RightHandMiddle3/b_RightHandMiddle4
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandMiddle1/b_RightHandMiddle2/b_RightHandMiddle3/b_RightHandMiddle4/b_RightHandMiddle4_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandMiddle1/b_RightHandMiddle2/b_RightHandMiddle3/b_RightHandMiddle4/b_RightHandMiddle4_end/b_RightHandMiddle4_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandPinky1
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandPinky1/b_RightHandPinky2
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandPinky1/b_RightHandPinky2/b_RightHandPinky3
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandPinky1/b_RightHandPinky2/b_RightHandPinky3/b_RightHandPinky4
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandPinky1/b_RightHandPinky2/b_RightHandPinky3/b_RightHandPinky4/b_RightHandPinky4_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandPinky1/b_RightHandPinky2/b_RightHandPinky3/b_RightHandPinky4/b_RightHandPinky4_end/b_RightHandPinky4_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandRing1
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandRing1/b_RightHandRing2
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandRing1/b_RightHandRing2/b_RightHandRing3
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandRing1/b_RightHandRing2/b_RightHandRing3/b_RightHandRing4
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandRing1/b_RightHandRing2/b_RightHandRing3/b_RightHandRing4/b_RightHandRing4_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandRing1/b_RightHandRing2/b_RightHandRing3/b_RightHandRing4/b_RightHandRing4_end/b_RightHandRing4_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandThumb1
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandThumb1/b_RightHandThumb2
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandThumb1/b_RightHandThumb2/b_RightHandThumb3
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandThumb1/b_RightHandThumb2/b_RightHandThumb3/b_RightHandThumb4
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandThumb1/b_RightHandThumb2/b_RightHandThumb3/b_RightHandThumb4/b_RightHandThumb4_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightHandThumb1/b_RightHandThumb2/b_RightHandThumb3/b_RightHandThumb4/b_RightHandThumb4_end/b_RightHandThumb4_end_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightWeapon
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightWeapon/b_RightWeapon_end
    m_Weight: 1
  - m_Path: Ironman/b_Root/b_Hips/b_Spine/b_Spine1/b_RightShoulder/b_RightArm/b_RightArmRoll/b_RightForeArm/b_RightForeArmRoll/b_RightHand/b_RightWeapon/b_RightWeapon_end/b_RightWeapon_end_end
    m_Weight: 1
  - m_Path: Ironman.001
    m_Weight: 1
