﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class TitleManager : MonoBehaviour
{
    //[SerializeField]
    //private Account account;
    [SerializeField]
    private InputField inputField = default;

    [SerializeField]
    private GameObject gameObject = default;

    public DataBeseManager dataBese;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void EndInput()
    {
        Account.PlayerName= inputField.text;
        Debug.Log(Account.PlayerName);
    }
    public void OnClickStartButton()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void OnClickRanking()
    {
        gameObject.SetActive(true);
        dataBese.ServerAccess2();

    }
    public void clouseClickRanking()
    {
        gameObject.SetActive(false);
    }

}
