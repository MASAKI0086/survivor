﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleSpawn : MonoBehaviour
{
    public GameObject[] Spawnpoint;
    private GameObject Plyer;
    // private GameObject[] Enemy;
    public GameObject Plyerprefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void charaSpawn()
    {
        int random = Random.Range(0, Spawnpoint.Length);
        Plyer = (GameObject)Instantiate(Plyerprefab, Spawnpoint[random].transform.position, Spawnpoint[random].transform.rotation) as GameObject;
        Plyer.name = "TitlePlayer";

        
    }
}
