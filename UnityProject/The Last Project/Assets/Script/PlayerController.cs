﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Camera ThirdPersonCamera;
    public Camera FirstPersonCamera;
    public int hitPoint = 100;  //HP
    //アニメーター
    [SerializeField]
    private Animator animator;

    //　rigidbody
    [SerializeField]
    private Rigidbody rigidbody = default;

    private Vector3 velocity;
    [SerializeField]
    private float jumpPower = 5f;
    //　レイを飛ばす体の位置
    [SerializeField]
    private Transform charaRay;
    //　レイの距離
    [SerializeField]
    private float charaRayRange = 0.2f;
    //　レイが地面に到達しているかどうか
    private bool isGround;
    //　入力値
    private Vector3 input;
    //　歩く速さ
    [SerializeField]
    private float walkSpeed = 1.5f;

    private bool isGroundCollider = false;
    //　レイを飛ばす距離
    [SerializeField]
    private float stepDistance = 0.5f;
    //　昇れる段差
    [SerializeField]
    private float stepOffset = 0.3f;
    //　昇れる角度
    [SerializeField]
    private float slopeLimit = 65f;
    //　昇れる段差の位置から飛ばすレイの距離
    [SerializeField]
    private float slopeDistance = 1f;
    // ヒットした情報を入れる場所
    private RaycastHit stepHit;

    [SerializeField]
    private GameObject Bullet;
    [SerializeField]
    private GameObject Bulletprefab;

    public GameObject gamemane;

    private bool isPrepare;

    public bool IsPrepare
    {
        get { return isPrepare; }
    }
    private bool isAlive;
    public bool IsAlive
    {
        get
        {
            return isAlive;
        }
    }
    [SerializeField]
    private Gamemanager Gamemanager;

    public GameObject firstcanvasobj;
    public GameObject Thirdcanvasobj;

    private Canvas firstcanvas;
    private Canvas Thirdcanvas;

    private SpawnManager SpawnManager;

    void Start()
    {
        //animator = GetComponent<Animator>();
        velocity = Vector3.zero;
        isGround = false;
        FirstPersonCamera.enabled = false;
        isPrepare = false;
        Bullet.gameObject.SetActive(false);
        gamemane = GameObject.Find("GameManager");
        Gamemanager = gamemane.GetComponent<Gamemanager>();
        firstcanvasobj = GameObject.Find("FirstPersonCanvas");
        firstcanvas = firstcanvasobj.GetComponent<Canvas>();
        firstcanvas.worldCamera = FirstPersonCamera;
        Thirdcanvasobj = GameObject.Find("ThirdPersonCanvas");
        Thirdcanvas=Thirdcanvasobj.GetComponent<Canvas>();
        Thirdcanvas.worldCamera = ThirdPersonCamera;
        SpawnManager = gamemane.GetComponent<SpawnManager>();
        isAlive = true;
       
       
        //rigid = GetComponent<Rigidbody>();
    }

    void Update()
    {
        CheckedRay();
        if (Gamemanager.Locked == false)
        {
           
            CharaControl();
            
            //HPが0になったときに敵を破壊する
            if (hitPoint <= 0)
            {
                isAlive = false;
                gameObject.transform.Find("Ironman.001").gameObject.SetActive(false);
               // Destroy(gameObject);
            }
        }
        GravityCotrol();
        if (-30 >= this.gameObject.transform.position.y)
        {
            int random2 = Random.Range(0, SpawnManager.Spawnpoint.Length);
            int random3 = Random.Range(0, 7);
            int random4 = Random.Range(0, 7);
            Vector3 posi = new Vector3(SpawnManager.Spawnpoint[random2].transform.position.x + random3, SpawnManager.Spawnpoint[random2].transform.position.y, SpawnManager.Spawnpoint[random2].transform.position.z + random4);
            // Destroy(gameObject);
            //  Gamemanager.character -= 1;
        }
    }
    void FixedUpdate()
    {
        //　キャラクターを移動させる処理
        rigidbody.MovePosition(transform.position + velocity * Time.deltaTime);
    }


    void CheckedRay()
    {
        //　キャラクターが接地していない時はレイを飛ばして確認
        if (!isGroundCollider)
        {
            if (Physics.Linecast(charaRay.position, (charaRay.position - transform.up * charaRayRange)))
            {
                isGround = true;
                rigidbody.useGravity = true;
            }
            else
            {
                isGround = false;
                rigidbody.useGravity = false;
            }
            Debug.DrawLine(charaRay.position, (charaRay.position - transform.up * charaRayRange), Color.red);
            //  Debug.Log("CharaRay" + charaRay.position + ":" +( charaRay.position - transform.up * charaRayRange));
        }
    }

    void CharaControl()
    {
        //　キャラクターコライダが接地、またはレイが地面に到達している場合
        if (isGroundCollider || isGround)
        {
            //　地面に接地してる時は初期化
            if (isGroundCollider)
            {
                velocity = Vector3.zero;

                //　着地していたらアニメーションパラメータと２段階ジャンプフラグをfalse
                animator.SetBool("isHumanoid_Jump", false);
                rigidbody.useGravity = true;

                //　レイを飛ばして接地確認の場合は重力だけは働かせておく、前後左右は初期化
            }
            else
            {
                velocity = new Vector3(0f, velocity.y, 0f);
            }

            input = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));

            //　方向キーが多少押されている
            if (input.magnitude > 0f)
            {
                animator.SetBool("isHumanoid_Walk", true);

                transform.LookAt(transform.position + input);

                //　昇れる段差を表示
                Debug.DrawLine(transform.position + new Vector3(0f, stepOffset, 0f), transform.position + new Vector3(0f, stepOffset, 0f) + transform.forward * slopeDistance, Color.green);

                //　ステップ用のレイが地面に接触しているかどうか
                if (Physics.Linecast(charaRay.position, charaRay.position + charaRay.forward * stepDistance, out stepHit, LayerMask.GetMask("Ground")))
                {

                    //　進行方向の地面の角度が指定以下、または昇れる段差より下だった場合の移動処理

                    if (Vector3.Angle(transform.up, stepHit.normal) <= slopeLimit
                        || (Vector3.Angle(transform.up, stepHit.normal) > slopeLimit
                            && !Physics.Linecast(transform.position + new Vector3(0f, stepOffset, 0f), transform.position + new Vector3(0f, stepOffset, 0f) + transform.forward * slopeDistance, LayerMask.GetMask("Ground")))
                    )
                    {
                        velocity = new Vector3(0f, (Quaternion.FromToRotation(Vector3.up, stepHit.normal) * transform.forward * walkSpeed).y, 0f) + transform.forward * walkSpeed;
                        Debug.Log(Vector3.Angle(transform.up, stepHit.normal));

                    }
                    else
                    {
                        velocity += transform.forward * walkSpeed;
                    }

                    Debug.Log(Vector3.Angle(Vector3.up, stepHit.normal));

                    //　ステップ用のレイが地面に接触していなければ
                }
                else
                {
                    velocity += transform.forward * walkSpeed;
                }
                //　キーの押しが小さすぎる場合は移動しない

                //if (Input.GetButton("Fire3"))
                //{
                //    FirstPersonCamera.enabled = true;
                //    ThirdPersonCamera.enabled = false;
                //    animator.SetBool("isHumanoid_WalkExt", true);
                //    //  Canvas.GetComponent<Canvas>().worldCamera = FirstPersonCamera;
                //}
                //else
                //{
                //    FirstPersonCamera.enabled = false;
                //    ThirdPersonCamera.enabled = true;
                //    animator.SetBool("isHumanoid_WalkExt", false);
                //    //Canvas.GetComponent<Canvas>().worldCamera = ThirdPersonCamera;
                //}
            }
            else
            {
                animator.SetBool("isHumanoid_Walk", false);
            }

            //　ジャンプ
            if (Input.GetButtonDown("Jump")
                && !animator.GetCurrentAnimatorStateInfo(0).IsName("Humanoid_Jump")
                && !animator.IsInTransition(0)      //　遷移途中にジャンプさせない条件
            )
            {
                animator.SetBool("isHumanoid_Jump", true);
                velocity.y += jumpPower;
                rigidbody.useGravity = false;
            }
            //if(Input.GetButton("Fire3"))
            //{
            //    FirstPersonCamera.enabled = true;
            //    ThirdPersonCamera.enabled = false;
            //    animator.SetBool("isHUmanoid_fire", true);
            //    //  Canvas.GetComponent<Canvas>().worldCamera = FirstPersonCamera;
            //}
            //else
            //{
            //    FirstPersonCamera.enabled = false;
            //    ThirdPersonCamera.enabled = true;
            //    animator.SetBool("isHUmanoid_fire", false);
            //    //Canvas.GetComponent<Canvas>().worldCamera = ThirdPersonCamera;
            //}
            if (Input.GetButton("Fire3"))
            {
                FirstPersonCamera.enabled = true;
                ThirdPersonCamera.enabled = false;
                animator.SetBool("isHumanoid_WalkExt", true);
                 // Canvas.GetComponent<Canvas>().worldCamera = FirstPersonCamera;
                isPrepare = true;
                Bullet.gameObject.SetActive(true);
                
            }
            else
            {
                FirstPersonCamera.enabled = false;
                ThirdPersonCamera.enabled = true;
                animator.SetBool("isHumanoid_WalkExt", false);
                isPrepare = false;
                Bullet.gameObject.SetActive(false);
                //Canvas.GetComponent<Canvas>().worldCamera = ThirdPersonCamera;
            }
            //if(isPrepare)
            //{
            //    if(Input.GetButtonDown("Fire2"))
            //    {

            //    }
            //}

        }
    }

    void GravityCotrol()
    {
        if (!isGroundCollider && !isGround)
        {
            velocity.y += Physics.gravity.y * Time.deltaTime;
        }
    }

    void OnCollisionEnter()
    {

        Debug.DrawLine(charaRay.position, charaRay.position + Vector3.down, Color.blue);

        //　他のコライダと接触している時は下向きにレイを飛ばしFieldかBlockレイヤーの時だけ接地とする
        if (Physics.Linecast(charaRay.position, charaRay.position + Vector3.down, LayerMask.GetMask("Ground")))
        {
            isGroundCollider = true;
            //Debug.Log("地面についtwマス");
        }
        else
        {
            isGroundCollider = false;
           // Debug.Log("地面についてません");
        }
    }

    //　接触していなければ空中に浮いている状態
    void OnCollisionExit()
    {
        isGroundCollider = false;

    }
    //ダメージを受け取ってHPを減らす関数
    public void Damage(int damage)
    {
        //受け取ったダメージ分HPを減らす
        hitPoint -= damage;
    }
}
