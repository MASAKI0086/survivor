﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Gamemanager : MonoBehaviour  
{

    public enum Game_State
    {
        Opening,
        Playing,
        Clear,
        Over,
        Null
    }
    private Game_State Game_state;
    private Game_State current_Game_state;

    public Game_State GameState
    {
        get { return Game_state; }
    }

    private bool locked;

    public bool Locked
    {
        get
        {
            return locked;
        }

        set
        {
            locked = value;
        }
    }

    [SerializeField]
    private GameObject TimeTextobj;
    [SerializeField]
    //private GameObject TimeTextobj2;
    //[SerializeField]
    //private GameObject HPTextobj;
    public GameObject rankingobj;
    // [SerializeField]
    // private GameObject TimeTextobj3;
    private GameObject plyer;
    public Text timerText;
    public Text timerText2;
    public Text HPText;
    public Text RemainingText;
    public Text PlayerText;
    // public Text timerText3;

    //オープニングステート時の一回だけの処理の判定
    private bool isCalledone_OpeningState;

    private bool isCalledone_PlayingState;
    private bool isCalledone_ClearState;


    int seconds_tmp;
    int minute_tmp;

   // [SerializeField]
   // private GameObject GameObject;
    private TimeManager timeManager;
    //  [SerializeField]
   // public GameObject Enemyprefab;
    public int MaxCaracter = 10;
    public int character;
   // public Transform[] SpawnPoint;
   // public CharacterManager[] chara;
   public  SpawnManager spawn;
    private PlayerController PlayerController;
    public DataBeseManager beseManager;
    //private Shooting shooting;

    // Start is called before the first frame update
    void Start()
    {
      //  ChangeCurrentState(Game_State.Null);
        ChangeGameState(Game_State.Opening);
        isCalledone_OpeningState = false;
       isCalledone_PlayingState = false;
        isCalledone_ClearState = false;

        timeManager = new TimeManager();
        timeManager.Initialize();
        locked = true;
        character = MaxCaracter;
        PlayerText.text = Account.PlayerName;
        //Enemy.= MaxCaracter;
        rankingobj.gameObject.SetActive (false);
        //plyer = GameObject.Find("Player");
        //shooting = plyer.GetComponent<Shooting>();
    }

    // Update is called once per frame
    void Update()
    {
       
      Dispatch(Game_state);
 
    }

    void Dispatch(Game_State game_State)
    {    
       // if (  game_State != current_Game_state)
      //  {
          //  Debug.Log("Gamestate:" + Game_state + " curent" + current_Game_state);
            switch (game_State)
            {
                case Game_State.Opening:
                  //  ChangeCurrentState(game_State);
                    OpeningState();
                    
                    break;

                case Game_State.Playing:
                    //ChangeCurrentState(game_State);
                    PlayingState();
                    
                    break;
                case Game_State.Clear:
                    ClearState();
                    break;
                case Game_State.Over:
                    OverState();
                    break;
            }
      //  }
    }
    void ChangeCurrentState(Game_State current_State)
    {
        current_Game_state = current_State;
    }
    void ChangeGameState(Game_State game_State)
    {        
        Game_state = game_State;
    }

    void OpeningState()
    {

        if(!isCalledone_OpeningState)
        {
            Debug.Log("オープニングステートが呼ばれたよ");
            timeManager.Limit_Time = 5;
            
            isCalledone_OpeningState = true;
            spawn.charaSpawn();
            PlayerController = GameObject.Find("Player").GetComponent<PlayerController>();
            HPText.text = "残体力:" + PlayerController.hitPoint.ToString("D3");
            RemainingText.text = "生存者:"+character.ToString("D2") + "/" + MaxCaracter.ToString("D2");
            
            
        }
        timeManager.TimeDown_second();
        timerText.text = timeManager.Second.ToString();
        if (timeManager.Second == 0)
        {
            ChangeGameState(Game_State.Playing);
        }

    }

    void PlayingState()
    {
        if(!isCalledone_PlayingState)
        {
            locked = false;
            timerText.text = "戦闘開始";
            timeManager.Initialize_Second();
            isCalledone_PlayingState = true;
            //Debug.Log("プレイステートが呼ばれたよ");
            Invoke("TimeText", 3);
        }

        timeManager.TimeUp();
        //Debug.Log("total:" + (int)timeManager.TotalSecond);
       // Debug.Log("second:" + timeManager.Second);
      //  minute_tmp = timeManager.Minute;
      //  seconds_tmp = timeManager.Second;
        timerText2.text = "戦闘時間<" + timeManager.Hour.ToString() + ":" + timeManager.Minute.ToString("D2") + ":" + timeManager.Second.ToString("D2") + ">";// seconds.ToString();
        HPText.text = "残体力:" + PlayerController.hitPoint.ToString("D3");
        RemainingText.text = "生存者:" + character.ToString("D2") + "/" + MaxCaracter.ToString("D2");

        if (character==1)
        {
            ChangeGameState(Game_State.Clear);
        }
     if(PlayerController.IsAlive==false)
        {
            ChangeGameState(Game_State.Over);
        }
        
    }
    void TimeText()
    {
        TimeTextobj.gameObject.SetActive(false);
    }

    void ClearState()
    {
        if(!isCalledone_ClearState)
        {
            TimeTextobj.gameObject.SetActive(true);
            timerText.text = "戦闘終了";
            rankingobj.SetActive(true);
            beseManager.ServerAccess(timeManager);
            beseManager.ServerAccess2();
            isCalledone_ClearState = true;
           
        }
        

          Invoke("TimeText", 3);
        Invoke("Title", 10);



    }

    void OverState()
    {
        TimeTextobj.gameObject.SetActive(true);
        timerText.text = "戦闘終了";                                                                                                                                 //  Invoke("TimeText", 3);
        Invoke("Title", 10);
    }
    void Title()
    {
        SceneManager.LoadScene("TitleScene");
    }
}
