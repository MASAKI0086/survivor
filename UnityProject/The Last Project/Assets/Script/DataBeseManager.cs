﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using MiniJSON;
using UnityEngine.UI;


public class DataBeseManager : MonoBehaviour
{
    private string AccessPointURL = "http://localhost/TheLastProjectAPI/ranking/setRanking";
    private string AccessPointURL2= "http://localhost/TheLastProjectAPI/ranking/getRankings";
    //private bool Searching = false;
    public List<RankingData> rankingDatas = null;
    public Text Text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ServerAccess(TimeManager timeManager)
    {
        Access(timeManager);
    }
    private void Access(TimeManager timeManager)
    {
        StartCoroutine(PostServer(timeManager));
        StartCoroutine(GetJson(CallbackWebrequestSuccess2));
    }
    public void ServerAccess2()
    {
        Access2();
    }
    private void Access2()
    {
        
        StartCoroutine(GetJson(CallbackWebrequestSuccess2));
    }
    private IEnumerator PostServer(TimeManager timeManager,Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        //Debug.Log("total:" + (int)timeManager.TotalSecond);
        WWWForm form = new WWWForm();
         form.AddField("name", Account.PlayerName);
        //form.AddField("name", "TEST");
        int aa = (int)timeManager.TotalSecond;
        form.AddField("clearseconds", aa);
       
        UnityWebRequest www = UnityWebRequest.Post(AccessPointURL, form);
        yield return www.SendWebRequest();
        if (www.error != null)
        {
            //レスポンスエラーの場合
            Debug.LogError(www.error);
            //if (null != cbkFailed)
            //{
            //    cbkFailed();
            //}
            Debug.Log("サーバにアクセスしたけどエラー");

        }
        else
        if (www.isDone)
        {
            Debug.Log("サーバにアクセスしたよ");
            // リクエスト成功の場合
           // Debug.Log(string.Format("Success:{0}", www.downloadHandler.text));
            if (null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
            }
        }
    }
    private void CallbackWebrequestSuccess(string response)
    {
        //ディスプレイに成功と表示する。
       // Display_Text.text = "検索に成功しました\n変換中";
       // Searching = false;

        StartCoroutine(enumerator(response));



    }
    private IEnumerator enumerator(string response)
    {
       rankingDatas = RankingDataModel.Data_DeserializeFromJson(response);
        Debug.Log(rankingDatas);
        string result = "";
        int index = 0;
        foreach (var addressData in rankingDatas)
        {
            ++index;
            string name = addressData.Name;
            string time = addressData.ClearTime.ToString();
            string date = addressData.Date;
            result += string.Format(index +"位"+ Environment.NewLine + "Name:{0}"+ Environment.NewLine,name);
            result += string.Format("TIME:{0}" + Environment.NewLine, time);
           // result += string.Format("DATE:{0}" + Environment.NewLine, date);


        }
        yield return new WaitForSeconds(1.0f);
        Debug.Log(result);
        Text.text = result ;
    }

    private IEnumerator GetJson(Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        UnityWebRequest www = UnityWebRequest.Get(AccessPointURL2);


        yield return www.SendWebRequest();
        if (www.error != null)
        {
            //レスポンスエラーの場合
            Debug.LogError(www.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else
        if (www.isDone)
        {
            // リクエスト成功の場合
           // Debug.Log(string.Format("Success:{0}", www.downloadHandler.text));
            if (null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
            }
        }

    }
    private void CallbackWebrequestSuccess2(string response)
    {
        StartCoroutine(enumerator(response));
        Debug.Log("CallbackWebrequestSuccess");


    }
    
}
//public class RankingDataModel
//{
//    public static List<RankingData> Data_DeserializeFromJson(string _Json)
//    {

//        List<RankingData> rankingDatas = new List<RankingData>();
//        RankingData tmp_rankingData = null;
//        var json = Json.Deserialize(_Json) as Dictionary<string, object>;
//        var results = (IList)json["results"];

//        foreach (IDictionary dictionary in results)
//        {
//            tmp_rankingData = new RankingData();
//            {
//                Debug.Log(int.Parse(dictionary["id"].ToString()));
//                //tmp_rankingData.Id = int.Parse((string)dictionary["id"]);
//                // tmp_rankingData.Name = (string)dictionary["Name"];
//                // tmp_rankingData.ClearTime = int.Parse((string)dictionary["ClearSeconds"]);
//                //tmp_rankingData.Date = (string)dictionary["date"];
//                //if (dictionary.Contains("id")) 
//                //{
//                //    tmp_rankingData.Id =int.Parse((string) dictionary["id"]);
//                //   // tmp_rankingData.Id = (int)dictionary["id"];
//                //}

//                //if (dictionary.Contains("Name"))
//                //{
//                //    tmp_rankingData.Name = (string)dictionary["Name"];
//                //}
//                //if(dictionary.Contains("ClearSeconds"))
//                //{
//                //   // tmp_rankingData.ClearTime = int.Parse((string)dictionary["clearseconds"]);
//                //    tmp_rankingData.ClearTime = (int)dictionary["ClearSeconds"];
//                //}
//                //if(dictionary.Contains("date"))
//                //{
//                //    tmp_rankingData.Date = (string)dictionary["date"];
//                //}
//            }
//            rankingDatas.Add(tmp_rankingData);
//        }
//        return rankingDatas;
//    }
//}

//public class RankingData
//{
//    public int Id
//    {
//        get;
//        set;
//    }

//    public string Name
//    {
//        get;
//        set;
//    }
//    public int ClearTime
//    {
//        get;
//        set;
//    }
//    public string Date
//    {
//        get;
//        set;
//    }

//}