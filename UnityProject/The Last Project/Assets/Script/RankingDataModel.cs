﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiniJSON;

public class RankingDataModel
{
    public static List<RankingData> Data_DeserializeFromJson(string _Json)
    {

        List<RankingData> rankingDatas = new List<RankingData>();
        //RankingData tmp_rankingData = null;
        //  var json = Json.Deserialize(_Json) as Dictionary<string, object>;
        //var results = (IList)json["results"];
        IList jsonList = (IList)Json.Deserialize(_Json);

        foreach (IDictionary dictionary in jsonList)
        {
            RankingData tmp_rankingData = new RankingData();
            {
                tmp_rankingData.Id = int.Parse(dictionary["id"].ToString());

               // Debug.Log(tmp_rankingData.Id);
                tmp_rankingData.Name = dictionary["Name"].ToString();

               // Debug.Log(tmp_rankingData.Name);
                tmp_rankingData.ClearTime = int.Parse(dictionary["ClearSeconds"].ToString());
               // Debug.Log(tmp_rankingData.ClearTime);

                tmp_rankingData.Date = dictionary["date"].ToString();
               // Debug.Log(tmp_rankingData.Date);
                //tmp_rankingData.Id = int.Parse((string)dictionary["id"]);
                // tmp_rankingData.Name = (string)dictionary["Name"];
                // tmp_rankingData.ClearTime = int.Parse((string)dictionary["ClearSeconds"]);
                //tmp_rankingData.Date = (string)dictionary["date"];
                //if (dictionary.Contains("id")) 
                //{
                //    tmp_rankingData.Id =int.Parse((string) dictionary["id"]);
                //   // tmp_rankingData.Id = (int)dictionary["id"];
                //}

                //if (dictionary.Contains("Name"))
                //{
                //    tmp_rankingData.Name = (string)dictionary["Name"];
                //}
                //if(dictionary.Contains("ClearSeconds"))
                //{
                //   // tmp_rankingData.ClearTime = int.Parse((string)dictionary["clearseconds"]);
                //    tmp_rankingData.ClearTime = (int)dictionary["ClearSeconds"];
                //}
                //if(dictionary.Contains("date"))
                //{
                //    tmp_rankingData.Date = (string)dictionary["date"];
                //}
            }
            rankingDatas.Add(tmp_rankingData);
        }
        return rankingDatas;
    }
}

public class RankingData
{
    public int Id
    {
        get;
        set;
    }

    public string Name
    {
        get;
        set;
    }
    public int ClearTime
    {
        get;
        set;
    }
    public string Date
    {
        get;
        set;
    }

}