﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{

    public GameObject bulletPrefab;
    public float shotSpeed;
    public int shotCount = 30;
    private float shotInterval;
    [SerializeField]
    private PlayerController PlayerController;
    [SerializeField] Transform cameraTrans;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerController.IsPrepare)
        {
            Debug.Log("構えたよ");
            if (Input.GetButtonDown("Fire2"))
            {

                //shotInterval += 1;

                //if (shotInterval % 5 == 0 && shotCount > 0)
                //{
                if (shotCount > 0)
                {
                    shotCount -= 1;

                    GameObject bullet = (GameObject)Instantiate(bulletPrefab, transform.position, Quaternion.Euler(transform.parent.eulerAngles.x - 90, transform.parent.eulerAngles.y, 0));
                    Rigidbody bulletRb = bullet.GetComponent<Rigidbody>();
                    bulletRb.AddForce(transform.forward * shotSpeed);
                    bullet.tag = ("PlayerShell");

                    //射撃されてから3秒後に銃弾のオブジェクトを破壊する.

                    Destroy(bullet, 3.0f);
                }
               // }

            }
        }
        else
        {
            Vector3 angle = new Vector3(0, 0, 0);
            cameraTrans.localEulerAngles = angle;
        }
        
    }
}
