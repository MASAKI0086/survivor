﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager 
{
    //秒数の変数
    private float totalsecond;
    private float second;
    private float old_second;

    public int Second
    {
        get { return (int)second; }
    }

    public float TotalSecond
    {
        get { return totalsecond; }
    } 

    //分数の変数
    private int minute;
    private float old_minute;

    public int Minute
    {
        get { return minute; }
    }

    //時間の変数
    private int hour;
    private float old_hour;

    public int Hour
    {
        get { return hour; }
    }

    private float Limit_time=0;
    public float Limit_Time
    {
        get { return Limit_time; }
        set { Limit_time = value; }
    }

    public void Initialize()
    {
        Initialize_TotalSecond();
        Initialize_Minute();
        Initialize_Second();
        Initialize_Hour();
        Initialize_Limit_time();
    }
    public void Initialize_TotalSecond()
    {
        totalsecond = 0;
    }
    public void Initialize_Second()
    {
        second = 0;
    }
    public void Initialize_Minute()
    {
        minute = 0;
    }
    public void Initialize_Hour()
    {
        hour = 0;
    }
    public void Initialize_Limit_time()
    {
        Limit_time = 0;
    }

    public void TimeUp()
    {
        
        //秒数を増やす
        second += Time.deltaTime;
        totalsecond += Time.deltaTime;
        //秒数が60fを上回ったか
        if (second >= 60f)
        {
            //分数を足す
            minute++;
            
            //秒数を今の秒数から60引く
            second = second - 60;
        }
        if(minute>=60)
        {
            hour++;

            minute = 0;
        }

    }

    public void TimeDown_second()
    {
        Limit_time -= Time.deltaTime;

        second = (int)Limit_time;
    }
}
