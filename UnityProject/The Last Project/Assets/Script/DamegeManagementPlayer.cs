﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamegeManagementPlayer : MonoBehaviour
{
    public int damage;       //当たった部位毎のダメージ量
    [SerializeField]
    private GameObject player;   //敵オブジェクト
    [SerializeField]
    private PlayerController hp;              //HPクラス

    void Start()
    {
        //enemy = GameObject.Find("Enemy");   //敵情報を取得
        //   hp = enemy.GetComponent<>();      //HP情報を取得
    }

    void OnTriggerEnter(Collider other)
    {

        //ぶつかったオブジェクトのTagにShellという名前が書いてあったならば（条件）.
        if (other.CompareTag("Shell"))
        {

            //HPクラスのDamage関数を呼び出す
            hp.Damage(damage);

            //ぶつかってきたオブジェクトを破壊する.
            Destroy(other.gameObject);
        }

        if(other.CompareTag("Enemy"))
        {
            hp.Damage(10);
        }
    }
}
