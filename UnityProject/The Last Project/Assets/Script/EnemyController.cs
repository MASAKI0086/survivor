﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public int hitPoint = 100;  //HP
    public GameObject gamemane;
    private Gamemanager Gamemanager;
    private SpawnManager SpawnManager;
    private NavMeshAgent navMeshAgent;
    [SerializeField]
    private Animator animator;
    private  Vector3 posi;
    bool set;

    // Start is called before the first frame update
    void Start()
    {
        gamemane = GameObject.Find("GameManager");
        Gamemanager = gamemane.GetComponent<Gamemanager>();
        SpawnManager = gamemane.GetComponent<SpawnManager>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        set = false;
        posi = gameObject.transform.position;


    }
    // Update is called once per frame
    void Update()
    {

        //HPが0になったときに敵を破壊する
        if (hitPoint <= 0)
        {
            Destroy(gameObject);
            Gamemanager.character -= 1;

        }
        else
        {
            if (0 > this.gameObject.transform.position.y)
            {
                int random2 = Random.Range(0, SpawnManager.Spawnpoint.Length);
                int random3 = Random.Range(0, 7);
                int random4 = Random.Range(0, 7);
                Vector3 posi = new Vector3(SpawnManager.Spawnpoint[random2].transform.position.x + random3, SpawnManager.Spawnpoint[random2].transform.position.y, SpawnManager.Spawnpoint[random2].transform.position.z + random4);
                // Destroy(gameObject);
                //  Gamemanager.character -= 1;
            }
           
            if (Gamemanager.Locked == false)
            {
                if (set == false)
                {
                    Target_set();
                }
                Move();

                Check();
            }
        }

       
       
        
    }
    //ダメージを受け取ってHPを減らす関数
    public void Damage(int damage)
    {
        //受け取ったダメージ分HPを減らす
        hitPoint -= damage;
    }


    public void Target_set()
    {
        int random2 = Random.Range(0, SpawnManager.Spawnpoint.Length);
        posi = new Vector3(SpawnManager.Spawnpoint[random2].transform.position.x, gameObject.transform.position.y, SpawnManager.Spawnpoint[random2].transform.position.z);
        set = true;
    }
    public void Move()
    {
        if (navMeshAgent.pathStatus != NavMeshPathStatus.PathInvalid)
        {
            //navMeshAgentの操作
            navMeshAgent.SetDestination(posi);
        }
        
        animator.SetBool("isHumanoid_Walk", true);
    }
   public  void Check()
    {
        if(posi.x==gameObject.transform.position.x&&posi.z==gameObject.transform.position.z)
        {
            animator.SetBool("isHumanoid_Walk", false);
            set = false;
        }
    }

}
