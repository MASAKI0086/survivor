﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
   
    [SerializeField] Transform playerTrans;
 
    [Range(0.1f, 10f)]
    //カメラ感度、数値が大きいほどより直感的な操作が可能.
    public float lookSensitivity = 5f;
    [Range(0.1f, 1f)]
    //数値が大きいほどカメラが向きたい方向に向くまでの時間が長くなる.
    public float lookSmooth = 0.1f;

    public Vector2 MinMaxAngle = new Vector2(-65, 65);
    //　入力値
    private Vector3 input;

    private float yRot;
    private float xRot;

    private float currentYRot;
    private float currentXRot;

    private float yRotVelocity;
    private float xRotVelocity;
    private GameObject gamemane;

    //[SerializeField]
    private Gamemanager Gamemanager;

    private void Start()
    {
        gamemane = GameObject.Find("GameManager");

        Gamemanager = gamemane.GetComponent<Gamemanager>();
    }

    void Update()
    {
       // if(Gamemanager.Locked==false)
      //  {
            input = new Vector3(Input.GetAxis("Horizontal2"), Input.GetAxis("Vertical2"), 0f);

            //　方向キーが多少押されている
            if (input.magnitude > 0f)
            {

                yRot += input.x * lookSensitivity; //マウスの移動.
                xRot -= input.y * lookSensitivity; //マウスの移動.


                yRot = Mathf.Clamp(yRot, playerTrans.localEulerAngles.y + MinMaxAngle.x, playerTrans.localEulerAngles.y + MinMaxAngle.y);
                xRot = Mathf.Clamp(xRot, MinMaxAngle.x, MinMaxAngle.y);//上下の角度移動の最大、最小.


                currentXRot = Mathf.SmoothDamp(currentXRot, xRot, ref xRotVelocity, lookSmooth);
                currentYRot = Mathf.SmoothDamp(currentYRot, yRot, ref yRotVelocity, lookSmooth);

                transform.rotation = Quaternion.Euler(currentXRot, currentYRot, 0);

            }
      //  }
       
    }

}
