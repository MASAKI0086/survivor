﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class TitlePlayer : MonoBehaviour
{
    private Vector3 posi;
    private bool set;
    private NavMeshAgent navMeshAgent;
    public GameObject[] Spawnpoint;
    // Start is called before the first frame update
    void Start()
    {
        posi = gameObject.transform.position;
        navMeshAgent = GetComponent<NavMeshAgent>();
        //navMeshAgent = this.gameObject.GetComponent<NavMeshAgent>();
        set = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (0 > this.gameObject.transform.position.y)
        {
            int random2 = Random.Range(0, Spawnpoint.Length);
            int random3 = Random.Range(0, 7);
            int random4 = Random.Range(0, 7);
            posi = new Vector3(Spawnpoint[random2].transform.position.x, Spawnpoint[random2].transform.position.y, Spawnpoint[random2].transform.position.z);
            // Destroy(gameObject);
            //  Gamemanager.character -= 1;
        }
        if (set == false)
        {
            Target_set();
        }

        Move();

        Check();


    }
    public void Target_set()
    {
        int random2 = Random.Range(0, Spawnpoint.Length);
        posi = new Vector3(Spawnpoint[random2].transform.position.x, gameObject.transform.position.y, Spawnpoint[random2].transform.position.z);
        set = true;
    }
    public void Move()
    {
        if (navMeshAgent.pathStatus != NavMeshPathStatus.PathInvalid)
        {
            //navMeshAgentの操作
            navMeshAgent.SetDestination(posi);
        }
        

    }
    public void Check()
    {
        if (posi.x == gameObject.transform.position.x && posi.z == gameObject.transform.position.z)
        {

            set = false;
        }
    }
}

