-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.8-MariaDB
-- PHP のバージョン: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `techtest`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `ranking`
--

CREATE TABLE `ranking` (
  `id` int(11) NOT NULL,
  `Name` varchar(128) NOT NULL,
  `ClearSeconds` int(255) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `ranking`
--

INSERT INTO `ranking` (`id`, `Name`, `ClearSeconds`, `date`) VALUES
(1, 'NO NAME', 999, '2019-12-12'),
(2, 'NO NAME', 999, '2019-12-12'),
(3, 'NO NAME', 999, '2019-12-12'),
(4, 'NO NAME', 999, '2019-12-12'),
(5, 'NO NAME', 999, '2019-12-12'),
(6, 'NO NAME', 999, '2019-12-12'),
(7, 'NO NAME', 999, '2019-12-12'),
(8, 'NO NAME', 999, '2019-12-12'),
(9, 'NO NAME', 999, '2019-12-12'),
(10, 'NO NAME', 999, '2019-12-12'),
(11, 'NO NAME', 999, '2019-12-12'),
(12, 'NO NAME', 999, '2019-12-12'),
(13, 'NO NAME', 999, '2019-12-12'),
(14, 'NO NAME', 999, '2019-12-12'),
(15, 'NO NAME', 999, '2019-12-12'),
(16, 'NO NAME', 999, '2019-12-12'),
(17, 'NO NAME', 999, '2019-12-12'),
(18, 'NO NAME', 999, '2019-12-12'),
(19, 'NO NAME', 999, '2019-12-12'),
(20, 'NO NAME', 999, '2019-12-13'),
(21, 'NO NAME', 999, '2019-12-13'),
(22, 'NO NAME', 999, '2019-12-13'),
(23, 'NO NAME', 999, '2019-12-13'),
(24, 'NO NAME', 999, '2019-12-13'),
(25, 'NO NAME', 999, '2019-12-13'),
(26, 'NO NAME', 999, '2019-12-13'),
(27, 'NO NAME', 999, '2019-12-13'),
(28, 'teck', 318, '2019-12-13');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `ranking`
--
ALTER TABLE `ranking`
  ADD PRIMARY KEY (`id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `ranking`
--
ALTER TABLE `ranking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
